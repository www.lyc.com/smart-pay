package com.founder.service;

import java.util.Map;

public interface IPayNotifyService {

    String handleAliPayNotify(String mchId, String channelId, Map params);

    String handleWxPayNotify(String mchId, String channelId, String pay, String xmlResult);
}
