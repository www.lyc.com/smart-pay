package com.founder.core.dao;

import com.founder.core.domain.PayChannel;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Transactional
@Component
@Repository
public interface PayChannelRespository extends JpaRepository<PayChannel,Integer>, JpaSpecificationExecutor<PayChannel> {

    @Query(value = "from PayChannel where mchId = :mchId and channelId = :channelId")
    PayChannel findByMchIdAndChannelId(@Param("mchId") String mchId, @Param("channelId") String channelId);

    @Transactional
    @Modifying
    @Query(value = "update PayChannel set state = 0 where mchId = :mchId and channelId = :channelId")
    int jpa_updateState4Closed(@Param("mchId") String mchId, @Param("channelId") String channelId);

    @Transactional
    @Modifying
    @Query(value = "update PayChannel set state = 1 where mchId = :mchId and channelId = :channelId")
    int jpa_updateState4Open(@Param("mchId") String mchId, @Param("channelId") String channelId);

    @Transactional
    @Modifying
    @Query(value = "update PayChannel set state = 0 ")
    int jpa_updateAllState4Closed();

    @Transactional
    @Modifying
    @Query(value = "update PayChannel set state = 1 ")
    int jpa_updateAllState4Open();

    @Query(value = "from PayChannel where channelName = :channelName and channelId = :channelId")
    List<PayChannel> findAllByChannelNameAndChannelId(@Param("channelName") String channelName, @Param("channelId") String channelId);
}
